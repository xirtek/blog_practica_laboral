<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'web_rie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ei2LwsIa+@XbD__>FaqX@^h0,SQZxOy9SyR,4)LI(Hgv,@.PlER<[X{>N;xTa~ld' );
define( 'SECURE_AUTH_KEY',  '1W0(#7 D.y151esV3HG{i=N.p$Jzu`krpX>G05w=j`kb<01D},{|^fPL;/,qQV3]' );
define( 'LOGGED_IN_KEY',    'Zp/rW%SPap<qZpu0x6]4ffFw&x8G:oa<)Lduw,/YU_t{~rYMXwr$^jpa_qGoH1U?' );
define( 'NONCE_KEY',        '^pQB,4&c$TsPO4jz/tv[@pbTx12N?ykHFCE-%JFGW_fiZn-ELRBLYn,t~08cw}E)' );
define( 'AUTH_SALT',        '71kli9O.7jk>~Iu!(cX6&<<K#=k@%=nJ;cYUuo{qka@:*~6E~:%5#y VBYY;Q.BF' );
define( 'SECURE_AUTH_SALT', '-gNv:2VwlJ/L)h{NVuc>0lbCXi!].lt*#``iY^FGk(N3]x!w.,h2-)~?RblSF6eT' );
define( 'LOGGED_IN_SALT',   '2Rh#ahM0dZE[&(-aNX1@PLH>z`C-#=SqmTyuk%n+Y]T}3x.)$b2Arn*!ju1)$Vdx' );
define( 'NONCE_SALT',       '(EOvv[~bcgN&%o) @ |eD1jt}u}GEX&~QOBevTv}xT7!BZ eFut|nuPT+E d}%SL' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
